﻿namespace LD42.Common
{
    public class PersistentGameObjectSingleton<T> : GameObjectSingleton<T> where T : PersistentGameObjectSingleton<T>
    {
        private new void Awake()
        {
            base.Awake();

            DontDestroyOnLoad(gameObject);
        }
    }
}