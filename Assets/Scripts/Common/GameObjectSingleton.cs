﻿using UnityEngine;

namespace LD42.Common
{
    public class GameObjectSingleton<T> : MonoBehaviour where T : GameObjectSingleton<T>
    {
        public static T Instance { get; private set; }

        protected void Awake()
        {
            if (Instance == null)
            {
                Instance = (T) this;
            }
            else
            {
                Destroy(this);
            }
        }
    }
}