﻿using UnityEngine.SceneManagement;

namespace LD42.Loading
{
    public static class LoadingUtil
    {
        public static void Load(int index)
        {
            SceneManager.LoadScene(index);
        }

        public static void Load(string name)
        {
            SceneManager.LoadScene(name);
        }

        public static void LoadAsync(int index)
        {
            SceneManager.LoadSceneAsync(index);
        }

        public static void LoadAsync(string name)
        {
            SceneManager.LoadSceneAsync(name);
        }
    }
}
