﻿using UnityEngine;

namespace LD42.Loading
{
    public class TimedSceneLoader : MonoBehaviour
    {
        [SerializeField]
        private string sceneName = "";

        [SerializeField]
        [Range(0, 10)]
        private float delay = 5.0f;

        private void Start()
        {
            Invoke("Load", delay);
        }

        public void Load()
        {
            LoadingUtil.Load(sceneName);
        }
    }
}
