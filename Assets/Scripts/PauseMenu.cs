﻿using UnityEngine;
using UnityEngine.SceneManagement;
using LD42.Loading;

public class PauseMenu : MonoBehaviour
{
    private void Start()
    {
        GameManager.GamePauseToggled.AddListener(ToggleMenu);
        gameObject.SetActive(false);
    }

    private void ToggleMenu()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void Restart()
    {
        GameManager.Unpause();
        LoadingUtil.Load(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        GameManager.Pause();
        LoadingUtil.Load("menu");
    }
}
