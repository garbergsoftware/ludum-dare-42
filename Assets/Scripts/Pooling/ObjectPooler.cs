﻿using System.Collections.Generic;
using UnityEngine;
using LD42.Common;

public class ObjectPooler : GameObjectSingleton<ObjectPooler>
{
    private static readonly Dictionary<string, Queue<GameObject>> PoolDictionary = new Dictionary<string, Queue<GameObject>>();
    private static readonly Dictionary<string, GameObject> QueueParentDictionary = new Dictionary<string, GameObject>();

    public static GameObject Instantiate(GameObject obj)
    {
        return Instantiate(obj, obj.transform.position, obj.transform.rotation);
    }

    public static GameObject Instantiate(GameObject obj, Vector3 position, Quaternion rotation)
    {
        var name = obj.name;

        if (PoolDictionary.ContainsKey(name))
        {
            var queue = PoolDictionary[name];
            if (queue.Count > 0)
            {
                return DequeueObject(queue, position, rotation);
            }
            else
            {
                return GameObject.Instantiate(obj, position, rotation);
            }
        }
        else
        {
            var queue = new Queue<GameObject>();
            PoolDictionary.Add(name, queue);

            CreateQueueParentObject(name);

            return GameObject.Instantiate(obj, position, rotation);
        }
    }

    public static void Destroy(GameObject obj)
    {
        var name = obj.name;
        name = name.Remove(name.Length - 7);

        if (PoolDictionary.ContainsKey(name))
        {
            obj.SetActive(false);

            if (QueueParentDictionary.ContainsKey(name))
            {
                obj.transform.parent = QueueParentDictionary[name].transform;
            }
            else
            {
                CreateQueueParentObject(name);
            }

            PoolDictionary[name].Enqueue(obj);
        }
        else
        {
            GameObject.Destroy(obj);
        }
    }

    private static void CreateQueueParentObject(string name)
    {
        var queueParentObj = new GameObject(name + " Queue");
        queueParentObj.transform.parent = Instance.transform;

        QueueParentDictionary.Add(name, queueParentObj);
    }

    private static GameObject DequeueObject(Queue<GameObject> queue, Vector3 position, Quaternion rotation)
    {
        var obj = queue.Dequeue();

        obj.transform.position = position;
        obj.transform.rotation = rotation;
        obj.transform.parent = null;
        obj.SetActive(true);

        return obj;
    }
}
