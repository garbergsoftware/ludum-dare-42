﻿using System.Collections;
using UnityEngine;

public class BasicObjectSpawner : MonoBehaviour
{
    public bool powerOn = false;

    [SerializeField]
    [Range(0.01f, 10f)]
    private float timeBetweenSpawns = 1.0f;

    [SerializeField]
    [Range(0.01f, 10f)]
    private float timeMultFactor = 1.5f;

    public GameObject[] objectsToSpawn;

    private void Start()
    {
        StartCoroutine(Spawner());
    }

    private IEnumerator Spawner()
    {
        var timeLowerBound = timeBetweenSpawns / timeMultFactor;
        var timeUpperBound = timeBetweenSpawns * timeMultFactor;

        while (true)
        {
            while (powerOn)
            {
                if (objectsToSpawn != null && objectsToSpawn.Length > 0)
                {
                    var objectToSpawn = DetermineObjectToSpawn();
                    ObjectPooler.Instantiate(objectToSpawn, transform.position, RandomizeRotation());
                    GameManager.TrashbagSpawned.Invoke();
                }

                var lowerTime = timeLowerBound  / (GameManager.SpawnRate * (GameManager.IncineratedTrashBags + 1));
                var upperTime = timeUpperBound / (GameManager.SpawnRate * (GameManager.IncineratedTrashBags + 1));

                var timeToSpawn = Random.Range(lowerTime, upperTime);

                yield return new WaitForSeconds(timeToSpawn);
            }

            yield return new WaitForSeconds(1);
        }
    }

    private GameObject DetermineObjectToSpawn()
    {
        var rand = Random.Range(0, objectsToSpawn.Length);
        return objectsToSpawn[rand];
    }

    private Quaternion RandomizeRotation()
    {
        var rotation = transform.rotation;

        rotation.x = Random.Range(-180.0f, 180f);
        rotation.y = Random.Range(-180.0f, 180f);
        rotation.z = Random.Range(-180.0f, 180f);

        return rotation;
    }
}
