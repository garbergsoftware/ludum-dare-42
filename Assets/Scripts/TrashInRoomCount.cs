﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TrashInRoomCount : MonoBehaviour
{
    [SerializeField]
    private Text textObj;

    [SerializeField]
    private string mainText = "";

    private void Start()
    {
        if (textObj == null)
        {
            textObj = GetComponent<Text>();
        }

        GameManager.TrashbagSpawned.AddListener(UpdateCount);
        GameManager.IncinerateCountIncreased.AddListener(UpdateCount);
    }

    private void UpdateCount()
    {
        textObj.text = mainText + GameManager.ActiveTrashBags;
    }
}
