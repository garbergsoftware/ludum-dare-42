﻿using UnityEngine;

public class DestroyAfterAudio : MonoBehaviour
{
    private void OnEnable()
    {
        Invoke("Destroy", 3);
    }

    private void Destroy()
    {
        ObjectPooler.Destroy(gameObject);
    }
}
