﻿using UnityEngine;

namespace LD42.Audio
{
    public class DelayedAudioPlayer : MonoBehaviour
    {
        [SerializeField]
        [Range(0, 10)]
        private float delay = 1.0f;

        private void Start()
        {
            var source = GetComponent<AudioSource>();

            if (source != null)
            {
                AudioUtil.Play(source, delay);
            }
        }
    }
}

