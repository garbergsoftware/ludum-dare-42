﻿using UnityEngine;

public class AudioOnEnable : MonoBehaviour
{
    [SerializeField]
    private AudioSource source;

    private void Awake()
    {
        if (source == null)
        {
            source = GetComponent<AudioSource>();
        }
    }

    private void OnEnable()
    {
        if (source != null && source.clip != null)
        {
            source.Play();
        }
        else
        {
            ObjectPooler.Destroy(gameObject);
        }
    }
}
