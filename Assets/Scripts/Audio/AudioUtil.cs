﻿using UnityEngine;

namespace LD42.Audio
{
    public static class AudioUtil
    {
        public static void Play(AudioSource source, float delay = 0)
        {
            if (delay > 0)
            {
                source.PlayDelayed(delay);
            }
            else
            {
                source.Play();
            }
        }
    }

}