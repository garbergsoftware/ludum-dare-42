﻿using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour
{
    public static int ActiveTrashBags { get; private set; }
    public static int IncineratedTrashBags { get; private set; }
    public static float SpawnRate { get; private set; }
    public static bool Paused { get; private set; }

    public static UnityEvent TrashbagSpawned = new UnityEvent();
    public static UnityEvent TrashIncinerated = new UnityEvent();
    public static UnityEvent IncinerateCountIncreased = new UnityEvent();
    public static UnityEvent GamePauseToggled = new UnityEvent();

    public GameManager()
    {
        SpawnRate = 1.0f;
    }

    private void Start()
    {
        TrashbagSpawned.AddListener(IncreaseActiveTrashCount);
        TrashIncinerated.AddListener(IncreaseIncineratedCount);
    }

    private void Update()
    {
        if (!Paused && Input.GetKeyDown(KeyCode.P))
        {
            Pause();
        }
        else
        {
            if (Paused && Input.GetKeyDown(KeyCode.P))
            {
                Unpause();
            }
        }
    }

    public static void Pause()
    {
        Time.timeScale = 0;
        Paused = true;
        MouseLook.ToggleCursorLock();
        Cursor.visible = true;
        GamePauseToggled.Invoke();
    }

    public static void Unpause()
    {
        Time.timeScale = 1;
        Paused = false;
        MouseLook.ToggleCursorLock();
        Cursor.visible = false;
        GamePauseToggled.Invoke();
    }

    private void IncreaseIncineratedCount()
    {
        IncineratedTrashBags++;
        ActiveTrashBags--;
        IncinerateCountIncreased.Invoke();
    }

    private void IncreaseActiveTrashCount()
    {
        ActiveTrashBags++;
    }
}
