﻿using UnityEngine;

public class Incinerator : MonoBehaviour {
    private void OnCollisionEnter(Collision col)
    {
        var obj = col.gameObject;
        Incinerate(obj);
    }

    private void OnTriggerEnter(Collider col)
    {
        var obj = col.gameObject;
        Incinerate(obj);
    }

    private static void Incinerate(GameObject obj)
    {  
        if (obj.CompareTag("Trash"))
        {      
            GameManager.TrashIncinerated.Invoke();
            if (Hand.CurrentlyHeldRigidbody == obj)
            {
                Hand.CurrentlyHeldRigidbody = null;
            }
            ObjectPooler.Destroy(obj);
        }
    }
}
