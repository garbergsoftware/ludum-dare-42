﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public static Rigidbody CurrentlyHeldRigidbody = null;

    private void Update()
    {
        if (CurrentlyHeldRigidbody != null && Input.GetMouseButtonDown(1))
        {
            Throw();
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        CheckForGrab(col);
    }

    private void OnTriggerStay(Collider col)
    {
        CheckForGrab(col);
    }

    private void CheckForGrab(Collider col)
    {
        var obj = col.gameObject;

        if (CurrentlyHeldRigidbody == null && Input.GetMouseButtonDown(0) && obj.CompareTag("Trash"))
        {
            Grab(obj);
        }
    }

    private void Grab(GameObject obj)
    {
        CurrentlyHeldRigidbody = obj.GetComponent<Rigidbody>();
        CurrentlyHeldRigidbody.isKinematic = true;
        obj.transform.parent = transform;
        obj.transform.position = transform.position;
    }

    private void Throw()
    {
        CurrentlyHeldRigidbody.transform.parent = null;
        CurrentlyHeldRigidbody.isKinematic = false;
        CurrentlyHeldRigidbody = null;
        //Exert force
    }
}
