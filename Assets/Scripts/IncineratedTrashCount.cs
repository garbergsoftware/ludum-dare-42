﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IncineratedTrashCount : MonoBehaviour
{
    [SerializeField]
    private Text textObj;

    [SerializeField]
    private string mainText = "";

    private void Start()
    {
        if (textObj == null)
        {
            textObj = GetComponent<Text>();
        }

        GameManager.IncinerateCountIncreased.AddListener(UpdateCount);
    }

    private void UpdateCount()
    {
        textObj.text = mainText + GameManager.IncineratedTrashBags;
    }
}
