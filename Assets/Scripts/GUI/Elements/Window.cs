﻿using UnityEngine;

namespace LD42.GUI.Elements
{
    public class Window : MonoBehaviour
    {
        public void Open()
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
        }

        public void Close()
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }
    }
}