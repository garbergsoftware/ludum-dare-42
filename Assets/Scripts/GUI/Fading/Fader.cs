﻿using UnityEngine;
using UnityEngine.UI;

namespace LD42.GUI.Fading
{
    public class Fader : MonoBehaviour
    {
        [SerializeField]
        [Range(0, 1)]
        private float endValue = 1.0f;

        [SerializeField]
        [Range(0, 10)]
        private float delay = 0;

        private void Start()
        {
            var graphic = GetComponent<Graphic>();

            if (graphic != null)
            {
                StartCoroutine(FadingUtil.Fade(graphic, endValue, delay));
            }
            else
            {
                Destroy(this);
            }
        }
    }
}