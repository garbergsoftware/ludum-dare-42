﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace LD42.GUI.Fading
{
    public static class FadingUtil
    {
        public static IEnumerator Fade(Graphic graphic, float endValue, float delay)
        {
            yield return new WaitForSecondsRealtime(delay);

            var color = graphic.color;

            var direction = color.a < endValue ? FadeDirection.In : FadeDirection.Out;

            yield return direction == FadeDirection.In ? FadeIn(color, graphic, endValue) : FadeOut(color, graphic, endValue);
        }

        private static IEnumerator FadeIn(Color color, Graphic graphic, float endValue)
        {
            for (var a = color.a; a <= endValue; a += 0.01f)
            {
                AssignColor(a, color, graphic);
                yield return null;
            }
        }

        private static IEnumerator FadeOut(Color color, Graphic graphic, float endValue)
        {
            for (var a = color.a; a >= endValue; a -= 0.01f)
            {
                AssignColor(a, color, graphic);
                yield return null;
            }
        }

        private static void AssignColor(float alpha, Color color, Graphic graphic)
        {
            color.a = alpha;
            graphic.color = color;
        }
    }
}