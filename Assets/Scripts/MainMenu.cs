﻿using LD42.Loading;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject mainMenu, instructions;

    public void StartGame()
    {
        LoadingUtil.Load("game");
    }

    public void OpenInstructions()
    {
        mainMenu.SetActive(false);
        instructions.SetActive(true);
    }

    public void CloseInstructions()
    {
        instructions.SetActive(false);
        mainMenu.SetActive(true);
    }
}
